//
//  NSColor+ColorClock.h
//  Color Clock Wallpaper
//
//  Created by Martin Pittenauer on 24.03.14.
//  Copyright (c) 2014 TheCodingMonkeys. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSColor (ColorClock)
+ (NSColor *) nowColor;
@end
