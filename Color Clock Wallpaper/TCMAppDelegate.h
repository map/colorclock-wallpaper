//
//  TCMAppDelegate.h
//  Color Clock Wallpaper
//
//  Created by Martin Pittenauer on 24.03.14.
//  Copyright (c) 2014 TheCodingMonkeys. All rights reserved.
//

#import <Cocoa/Cocoa.h>
NSString * const TCMNSScreenNumberKey = @"NSScreenNumber";

@interface TCMAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (strong) NSTimer *timer;
@property (strong) NSMutableDictionary *windows;

@end
