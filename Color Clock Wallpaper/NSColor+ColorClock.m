//
//  NSColor+ColorClock.m
//  Color Clock Wallpaper
//
//  Created by Martin Pittenauer on 24.03.14.
//  Copyright (c) 2014 TheCodingMonkeys. All rights reserved.
//

#import "NSColor+ColorClock.h"

@implementation NSColor (ColorClock)

+ (NSColor *) nowColor {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents *utcComponents = [calendar components:(NSHourCalendarUnit| NSMinuteCalendarUnit) fromDate:[NSDate date]];
    
    NSInteger currentHour = [utcComponents hour];
    NSInteger nextHour = (currentHour + 1) % 24;
    
    
    NSArray *colors = @[@[@0xff, @0x00, @0x00], @[@0xbd, @0x1f, @0x1f],@[@0xff, @0x50, @0x00],@[@0xff, @0x8b, @0x00],@[@0xff, @0xc5, @0x00],@[@0xff, @0xff, @0x00],@[@0xd0, @0xd8, @0x1e],@[@0xa2, @0xb1, @0x3d],@[@0x80, @0xff, @0x7a],@[@0x00, @0xff, @0x00],@[@0x02, @0xc0, @0x00],@[@0x00, @0x72, @0x0e],@[@0x88, @0x88, @0x88],@[@0x00, @0xff, @0xff],@[@0x00, @0x80, @0x80],@[@0x00, @0x66, @0xbb],@[@0x00, @0x00, @0xff],@[@0x00, @0x00, @0x80],@[@0x38, @0x23, @0xd2],@[@0x70, @0x07, @0xa6],@[@0xac, @0x6f, @0xd5],@[@0xb0, @0x03, @0x55],@[@0xff, @0x00, @0xff],@[@0xff, @0x36, @0x7f]];
    
    
    CGFloat t = [utcComponents minute] / 60.0;
    CGFloat r0 = [colors[currentHour][0] floatValue];
    CGFloat g0 = [colors[currentHour][1] floatValue];
    CGFloat b0 = [colors[currentHour][2] floatValue];
    CGFloat r1 = [colors[nextHour][0] floatValue];
    CGFloat g1 = [colors[nextHour][1] floatValue];
    CGFloat b1 = [colors[nextHour][2] floatValue];
    CGFloat r = (1.0 - t) * r0 + t * r1;
    CGFloat g = (1.0 - t) * g0 + t * g1;
    CGFloat b = (1.0 - t) * b0 + t * b1;
    
    NSColor *timeColor = [NSColor colorWithCalibratedRed:r/255 green:g/255 blue:b/255 alpha:1];
    return timeColor;
}

@end
