//
//  TCMAppDelegate.m
//  Color Clock Wallpaper
//
//  Created by Martin Pittenauer on 24.03.14.
//  Copyright (c) 2014 TheCodingMonkeys. All rights reserved.
//

#import "TCMAppDelegate.h"
#import "NSColor+ColorClock.h"

@implementation TCMAppDelegate

- (NSWindow *)clockWindowForScreen:(NSScreen *)aScreen {
    NSWindow *clockWindow;
    
    NSRect frame = [aScreen frame];
    
    // just create an empty window at the back of the last screen
    clockWindow = [[NSWindow alloc] initWithContentRect:frame styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    [clockWindow setLevel:kCGDesktopWindowLevel];
    [clockWindow setReleasedWhenClosed:NO];
    [clockWindow setIgnoresMouseEvents:YES];
    [clockWindow setBackgroundColor:[NSColor nowColor]];
    
	if ( NSAppKitVersionNumber <= 1038 ) {//the system is 10.6 or older
		[clockWindow setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces | (1 << 4) ];
	} else {
		[clockWindow setCollectionBehavior:NSWindowCollectionBehaviorCanJoinAllSpaces | (1 << 3) ];
	}
    [clockWindow orderBack:self];
    return clockWindow;
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    self.windows = [NSMutableDictionary new];
    [self updateWindows:self];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(updateWindows:) userInfo:nil repeats:YES];
}

- (void)updateWindows:(id)caller {
    
    //Update windows
    
    for (NSScreen *screen in [NSScreen screens]) {
        NSNumber *screenID = [[screen deviceDescription] objectForKey:TCMNSScreenNumberKey];
        
        if (![self.windows objectForKey:screenID]) {
            NSWindow *newWindow = [self clockWindowForScreen:screen];
            [self.windows setObject:newWindow forKey:screenID];
        }
    }
    
    for (NSWindow *window in [self.windows allValues]) {
        window.backgroundColor = [NSColor nowColor];
    }

    
}

@end
